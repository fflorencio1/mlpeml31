# EML 3.1 - MNIST TensorFlow Model for MLServer

Este repositório contém o código e os artefatos para a atividade 1 do módulo 3 de Engenharia de Machine Learning, parte do curso de pós-graduação ML in Production da UFSCar. 
Aluno: Felipe de Almeida Florencio. ITI - UFSCar

This repository contains a Docker setup to host a TensorFlow model for the MNIST dataset using MLServer.

## Structure

- `Dockerfile`: Dockerfile to build the image.
- `requirements.txt`: Python dependencies.
- `settings.json`: MLServer settings.
- `models/mnist`: Directory containing the trained MNIST model.
- `src/mnist.py`: Script to train the MNIST model.
- `data`: Directory for data files if needed.

## Steps

1. Train the model:

    ```bash
    python src/mnist-classifier/mnist.py
    ```

2. Build the Docker image:

    ```bash
    docker build -t mnist-mlserver .
    ```

3. Run the Docker container:

    ```bash
    docker run -p 8080:8080 mnist-mlserver
    ```

4. The MLServer will be available at `http://localhost:8080`.

## Making Predictions

You can make predictions by sending a request to the server. Example using `curl`:

```bash
curl -X POST http://localhost:8080/v2/models/mnist-model/infer -d '{
  "inputs": [
    {
      "name": "input",
      "shape": [1, 28, 28],
      "datatype": "FP32",
      "data": [...]
    }
  ]
}'
